import { IRolePermissionCheck } from 'src/interface';
import roleService from './role.service';

const roleController = {
  rolePermissionCheck: async (payload: IRolePermissionCheck) => {
    const allowed = await roleService.rolePermissionCheck(payload);

    return { allowed };
  },
}

export default roleController;
