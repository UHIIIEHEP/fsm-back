import express, { Request, Response } from 'express';
import { userInfoMiddleware } from './../../middleware/user_info.middleware';

import { serverResponse } from './../../helper';
import roleController from './role.controller';
import { joiSchemaRolePermissionCheck } from './role.schema';

const router = express.Router();

router.post('/permission/check', userInfoMiddleware, async (req: Request, res: Response) => {
  try {
    await joiSchemaRolePermissionCheck.validateAsync(req.body);

    const data = await roleController.rolePermissionCheck(req.body);

    serverResponse(req, res, data);
  } catch (err) {
    serverResponse(req, res, err, 'error');
  }
})

export default router;
