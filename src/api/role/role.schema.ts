const joi = require('joi')
const j2s = require('joi-to-swagger')


// Joi

export const joiSchemaRolePermissionCheck = joi.object().keys({
  alias: joi.string().example('idea.read'),
  relation: joi.string().example('user'),
  relation_id: joi.number().example(1),
  userInfoBySession: [joi.any()],
})

// end of Joi


const schemaUserRolePermissionCheck = j2s(joiSchemaRolePermissionCheck).swagger;

export {
  schemaUserRolePermissionCheck,
}