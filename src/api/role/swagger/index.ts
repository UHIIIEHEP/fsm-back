import { swUserRolePermissionCheck } from './swUserRolePermissionCheck';


export const swUserRoute = {
  '/role/permission/check': {
    'post': {
      ...swUserRolePermissionCheck,
    },
  },
}
