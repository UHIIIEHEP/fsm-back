import commonResponse from '../../../swagger/commonResponse'

import { schemaUserRolePermissionCheck } from "../role.schema";

export const swUserRolePermissionCheck = {
  summary: 'Ролевая модель',
  tags: [
    'role'
  ],
  requestBody: {
    content: {
      'application/json': {
        schema: {
          ...schemaUserRolePermissionCheck,
        }
      }
    }
  },
  responses: {
    200: {
      description: 'Done',
    },
    404: commonResponse[404],
    default: {
      'description': 'Error message',
    },
  },
}