
import { databaseQuery, unAssocData } from './../../helper';
import { IRolePermissionCheck } from './../../interface';

require('dotenv').config();

const { Client } = require('pg')

const db = new Client({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
})
db.connect();

const roleService = {
  rolePermissionCheck: async (payload: IRolePermissionCheck) => {
    const {
      alias,
      relation,
      relation_id,
      userInfoBySession: { user_id },
    } = payload;

    const allowed = await databaseQuery(db, 'pkg_role', 'role_permission_check', [
      user_id,
      relation,
      relation_id,
      alias,
    ])

    console.log({ allowed });

    return true;
  }
}

export default roleService;