import { IUserAuth, IUserSelfInfo } from "src/interface";
import userService from "./user.service";

const userController = {
  auth: async (payload: IUserAuth, address: string, client: string) => {
    const token = await userService.auth(payload, address, client);

    return { token };
  },

  userSelfInfo: async (payload: { userInfoBySession: IUserSelfInfo }) => {
    const user = await userService.userSelfInfo(payload);

    return { user };
  },
}

export default userController;
