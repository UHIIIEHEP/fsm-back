const joi = require('joi')
const j2s = require('joi-to-swagger')


// Joi

export const joiSchemaAuth = joi.object().keys({
  email: joi.string().example('user@mail.ru'),
  password: joi.string().example('123456'),
})

export const joiSchemaSelfInfo = joi.object().keys({
  // userInfoBySession: [joi.any()],
})

// end of Joi


const schemaUserAuth = j2s(joiSchemaAuth).swagger;
const schemaUserSelfInfo = j2s(joiSchemaSelfInfo).swagger;

export {
  schemaUserAuth,
  schemaUserSelfInfo,
}
