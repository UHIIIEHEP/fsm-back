import * as jwt from 'jsonwebtoken';
import { databaseQuery, unAssocData } from './../../helper';
import { IUserAuth, IUserSelfInfo } from './../../interface';

require('dotenv').config();

const { Client } = require('pg')

const db = new Client({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
})
db.connect()

const userService = {
  auth: async (payload: IUserAuth, address: string, client: string) => {
    const {
      email,
      password,
    } = payload;

    const session = await databaseQuery(db, 'pkg_user', 'user_auth', [
      email,
      password,
      address,
      client,
      process.env.APP_USER_PASSWORD_SALT,
    ]);

    const session_id = session[0].user_auth

    const token = await jwt.sign(
      {
        session_id,
      },
      process.env.APP_USER_JWT_SIGNATURE || '',
    );

    return token;
  },

  userSelfInfo: async (payload: { userInfoBySession: IUserSelfInfo }) => {
    const {
      userInfoBySession: { user_id },
    } = payload;

    const user = await databaseQuery(db, 'pkg_user', 'user_list', [[user_id]]);

    return user[0];
  },
}

export default userService;