import express, { Request, Response } from 'express';
import { userInfoMiddleware } from './../../middleware/user_info.middleware';

import { serverResponse } from './../../helper';
import userController from './user.controller';
import { joiSchemaAuth, joiSchemaSelfInfo } from './user.schema';

const router = express.Router();

router.post('/auth', async (req: Request, res: Response) => {
  try {
    await joiSchemaAuth.validateAsync(req.body);

    const client = req.headers['user-agent'] || '';
    const address = String(req.headers['x-real-ip'] || req.ip);

    const data = await userController.auth(req.body, address, client);

    serverResponse(req, res, data);
  } catch (err) {
    serverResponse(req, res, err, 'error');
  }
});

router.post('/self/info', userInfoMiddleware, async (req: Request, res: Response) => {
  try {
    // await joiSchemaSelfInfo.validateAsync(req.body);

    const data = await userController.userSelfInfo(req.body);

    serverResponse(req, res, data);
  } catch (err) {
    serverResponse(req, res, err, 'error');
  }
});

export default router;
