import commonResponse from './../../../swagger/commonResponse'

import { schemaUserSelfInfo } from "../user.schema";

export const swUserSelfInfo = {
  summary: 'Информация о себе',
  tags: [
    'User'
  ],
  requestBody: {
    content: {
      'application/json': {
        schema: {
          ...schemaUserSelfInfo,
        }
      }
    }
  },
  responses: {
    200: {
      description: 'Done',
    },
    404: commonResponse[404],
    default: {
      'description': 'Error message',
    },
  },
}