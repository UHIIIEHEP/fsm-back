import { swUserAuth } from "./swUserAuth";
import { swUserSelfInfo } from "./swUserSelfInfo";


export const swUserRoute = {
  '/user/auth': {
    'post': {
      ...swUserAuth,
    },
  },
  '/user/self/info': {
    'post': {
      ...swUserSelfInfo,
    },
  },
}
