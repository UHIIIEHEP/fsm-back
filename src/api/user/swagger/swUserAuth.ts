import commonResponse from './../../../swagger/commonResponse'

import { schemaUserAuth } from "../user.schema";

export const swUserAuth = {
  summary: 'Аутентификация',
  tags: [
    'User'
  ],
  requestBody: {
    content: {
      'application/json': {
        schema: {
          ...schemaUserAuth,
        }
      }
    }
  },
  responses: {
    200: {
      description: 'Done',
    },
    404: commonResponse[404],
    default: {
      'description': 'Error message',
    },
  },
}