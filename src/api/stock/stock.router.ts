import express, { Request, Response } from 'express';
import { userInfoMiddleware } from './../../middleware/user_info.middleware';

import { serverResponse } from './../../helper';
import stockController from './stock.controller';
import { joiSchemaList } from './stock.schema';

const router = express.Router();

// router.post('/create', async (req: Request, res: Response) => {
//   try {
//     await joiSchemaCreate.validateAsync(req.body)

//     const data = await stockController.create(req.body)

//     serverResponse(req, res, data);
//   } catch (err) {
//     serverResponse(req, res, err, 'error');
//   }
// });

router.post('/list', userInfoMiddleware, async (req: Request, res: Response) => {
  try {
    await joiSchemaList.validateAsync(req.body)

    const data = await stockController.list(req.body)

    serverResponse(req, res, data);
  } catch (err) {
    serverResponse(req, res, err, 'error');
  }
});


export default router;
