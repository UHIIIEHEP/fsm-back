import stockService from './stock.service';


const stockController = {
  // create: async (payload: any) => {

  //   const stock = await stockService.create(payload);

  //   return { stock };
  // },

  list: async (payload: any) => {

    const stock = await stockService.list(payload);

    return { stock };
  }
}

export default stockController;