import commonResponse from './../../../swagger/commonResponse'
import { schemaStockList } from '../stock.schema'

export const swStockList = {
  summary: 'Список ценных бумаг',
  tags: [
    'Stock'
  ],
  requestBody: {
    content: {
      'application/json': {
        schema: {
          ...schemaStockList
        }
      }
    }
  },
  responses: {
    200: {
      description: 'Done',
    },
    404: commonResponse[404],
    default: {
      'description': 'Error message',
    },
  },
}
