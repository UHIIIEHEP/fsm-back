// import { swStockCreate } from './swStockCreate';
import { swStockList } from './swStockList';

export const swStockRoute = {
  // "/stock/create": {
  //   "post": {
  //     ...swStockCreate,
  //   },
  // },
  "/stock/list": {
    "post": {
      ...swStockList,
    },
  },
}
