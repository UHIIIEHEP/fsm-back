
import { databaseQuery, unAssocData } from './../../helper';
import { IStock } from './../../interface';
require('dotenv').config();

const { Client } = require('pg')

const db = new Client({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
})
db.connect()

const stockService = {
  // create: async (payload: IStock) => {
  //   console.log({ payload })

  //   const user_id = 1;
  //   const {
  //     stock_id,
  //     amount,
  //     price_buy,
  //     price_sale,
  //     commission_percent,
  //   } = payload;

  //   const stock_id = await databaseQuery('pkg_stock', 'stock_create', [
  //     user_id,
  //     stock_id,
  //     amount,
  //     price_buy,
  //     price_sale,
  //     commission_percent,
  //   ]);

  //   console.log(111, stock_id[0].stock_create)

  //   const stock = await databaseQuery('pkg_stock', 'stock_list', [user_id, [stock_id.stock_create], stock_id]);

  //   console.log(222, { stock })

  //   return stock;
  // },

  list: async (payload: any) => {

    const user_id = 1;

    const { stock_id } = payload;

    const stock = await databaseQuery(db, 'pkg_stock', 'stock_list', [user_id, stock_id]);

    return unAssocData(stock);
  }
}

export default stockService;