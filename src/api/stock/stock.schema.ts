const joi = require('joi')
const j2s = require('joi-to-swagger')

// Joi

export const joiSchemaList = joi.object().keys({
  stock_id: [joi.number().example(null).optional(), joi.allow(null)],
  userInfoBySession: [joi.any()],
})

// end of Joi

const schemaStockList = j2s(joiSchemaList).swagger
export {
  schemaStockList,
}