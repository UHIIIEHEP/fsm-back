import commonResponse from './../../../swagger/commonResponse'
import { schemaIdeaDelete } from '../idea.schema'

export const swIdeaDelete = {
  summary: 'Удаление идеи',
  tags: [
    'Idea'
  ],
  requestBody: {
    content: {
      'application/json': {
        schema: {
          ...schemaIdeaDelete
        }
      }
    }
  },
  responses: {
    200: {
      description: 'Done',
    },
    404: commonResponse[404],
    default: {
      'description': 'Error message',
    },
  },
}
