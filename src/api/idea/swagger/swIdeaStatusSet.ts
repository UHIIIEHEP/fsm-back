import commonResponse from './../../../swagger/commonResponse'
import { schemaIdeaStatusSet } from '../idea.schema'

export const swIdeaStatusSet = {
  summary: 'Установка статуса',
  tags: [
    'Idea'
  ],
  requestBody: {
    content: {
      'application/json': {
        schema: {
          ...schemaIdeaStatusSet
        }
      }
    }
  },
  responses: {
    200: {
      description: 'Done',
    },
    404: commonResponse[404],
    default: {
      'description': 'Error message',
    },
  },
}
