import commonResponse from './../../../swagger/commonResponse'
import { schemaIdeaList } from '../idea.schema'

export const swIdeaList = {
  summary: 'Список идей',
  tags: [
    'Idea'
  ],
  requestBody: {
    content: {
      'application/json': {
        schema: {
          ...schemaIdeaList
        }
      }
    }
  },
  responses: {
    200: {
      description: 'Done',
    },
    404: commonResponse[404],
    default: {
      'description': 'Error message',
    },
  },
}
