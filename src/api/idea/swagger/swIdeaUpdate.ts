import commonResponse from './../../../swagger/commonResponse'
import { schemaIdeaUpdate } from '../idea.schema'

export const swIdeaUpdate = {
  summary: 'Обновление идеи',
  tags: [
    'Idea'
  ],
  requestBody: {
    content: {
      'application/json': {
        schema: {
          ...schemaIdeaUpdate
        }
      }
    }
  },
  responses: {
    200: {
      description: 'Done',
    },
    404: commonResponse[404],
    default: {
      'description': 'Error message',
    },
  },
}
