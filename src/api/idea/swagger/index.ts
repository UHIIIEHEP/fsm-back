import { swIdeaCreate } from './swIdeaCreate';
import { swIdeaDelete } from './swIdeaDelete';
import { swIdeaList } from './swIdeaList';
import { swIdeaStatusSet } from './swIdeaStatusSet';
import { swIdeaUpdate } from './swIdeaUpdate';

export const swIdeaRoute = {
  '/idea/create': {
    'post': {
      ...swIdeaCreate,
    },
  },
  '/idea/delete': {
    'post': {
      ...swIdeaDelete,
    },
  },
  '/idea/list': {
    'post': {
      ...swIdeaList,
    },
  },
  '/idea/status/set': {
    'post': {
      ...swIdeaStatusSet,
    },
  },
  '/idea/update': {
    'post': {
      ...swIdeaUpdate,
    },
  },
}
