import commonResponse from './../../../swagger/commonResponse'
import { schemaIdeaCreate } from '../idea.schema'

export const swIdeaCreate = {
  summary: 'Создание идеи',
  tags: [
    'Idea'
  ],
  requestBody: {
    content: {
      'application/json': {
        schema: {
          ...schemaIdeaCreate
        }
      }
    }
  },
  responses: {
    200: {
      description: 'Done',
    },
    404: commonResponse[404],
    default: {
      'description': 'Error message',
    },
  },
}
