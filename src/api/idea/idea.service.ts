
import { databaseQuery, unAssocData } from './../../helper';
import { IIdea } from './../../interface';
require('dotenv').config();

const { Client } = require('pg')

const db = new Client({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
})
db.connect()

const ideaService = {
  create: async (payload: IIdea) => {
    // const user_id = 1;
    const {
      stock_id,
      amount,
      price_buy,
      price_sale,
      commission_percent,
      userInfoBySession: { user_id },
    } = payload;

    const idea_id = await databaseQuery(db, 'pkg_idea', 'idea_create', [
      user_id,
      stock_id,
      amount,
      price_buy,
      price_sale,
      commission_percent,
    ]);

    const idea = await databaseQuery(db, 'pkg_idea', 'idea_list', [user_id, [idea_id.idea_create], stock_id]);

    return idea;
  },

  delete: async (payload: any) => {

    const user_id = 1;

    const { idea_id } = payload;

    const [idea] = await databaseQuery(db, 'pkg_idea', 'idea_delete', [user_id, idea_id]);

    return idea.idea_delete;
  },

  list: async (payload: any) => {

    const {
      idea_id,
      stock_id,
      userInfoBySession: { user_id },
    } = payload;

    const idea = await databaseQuery(db, 'pkg_idea', 'idea_list', [user_id, idea_id, stock_id]);

    return unAssocData(idea);
  },

  statusSet: async (payload: any) => {
    const user_id = 1;
    const {
      idea_id,
      status,
    } = payload;

    const ideaStatusSet = await databaseQuery(db, 'pkg_idea', 'idea_status_set', [
      user_id,
      idea_id,
      status,
    ]);

    const idea = await databaseQuery(db, 'pkg_idea', 'idea_list', [user_id, [ideaStatusSet[0].idea_status_set]]);

    return idea;
  },

  update: async (payload: any) => {
    const user_id = 1;
    const {
      idea_id,
      price_buy = null,
      price_sale = null,
      amount = null,
    } = payload;

    const ideaUpdate = await databaseQuery(db, 'pkg_idea', 'idea_update', [
      user_id,
      idea_id,
      price_sale,
      price_buy,
      amount,
    ]);

    const idea = await databaseQuery(db, 'pkg_idea', 'idea_list', [user_id, [ideaUpdate[0].idea_update]]);

    return idea;
  }
};

export default ideaService;
