const joi = require('joi')
const j2s = require('joi-to-swagger')
import fixture from '../../fixture';

// Joi
export const joiSchemaCreate = joi.object().keys({
  stock_id: joi.number().required().example(fixture.idea.stock_id),
  amount: joi.number().required().example(fixture.idea.amount),
  price_buy: joi.number().required().example(fixture.idea.price_buy),
  price_sale: joi.number().required().example(fixture.idea.price_sale),
  commission_percent: joi.number().required().example(fixture.idea.commission_percent),
  userInfoBySession: [joi.any()],
})
export const joiSchemaDelete = joi.object().keys({
  idea_id: joi.array().items(joi.number()).example([1, 5]),
  userInfoBySession: [joi.any()],
})

export const joiSchemaList = joi.object().keys({
  idea_id: [joi.array().items(joi.number()).optional().example(null), joi.allow(null)],
  stock_id: [joi.number().example(null).optional(), joi.allow(null)],
  userInfoBySession: [joi.any()],
})

export const joiSchemaStatusSet = joi.object().keys({
  idea_id: joi.number().example(1),
  status: joi.string().example('in_work'),
  userInfoBySession: [joi.any()],
})

export const joiSchemaUpdate = joi.object().keys({
  idea_id: joi.number().example(1),
  amount: [joi.array().items(joi.number()).optional().example(null), joi.allow(null)],
  price_buy: [joi.array().items(joi.number()).optional().example(null), joi.allow(null)],
  price_sale: [joi.array().items(joi.number()).optional().example(null), joi.allow(null)],
  userInfoBySession: [joi.any()],
})
// end of Joi

const schemaIdeaCreate = j2s(joiSchemaCreate).swagger
const schemaIdeaDelete = j2s(joiSchemaDelete).swagger
const schemaIdeaList = j2s(joiSchemaList).swagger
const schemaIdeaStatusSet = j2s(joiSchemaStatusSet).swagger
const schemaIdeaUpdate = j2s(joiSchemaUpdate).swagger
export {
  schemaIdeaCreate,
  schemaIdeaDelete,
  schemaIdeaList,
  schemaIdeaStatusSet,
  schemaIdeaUpdate,
}
