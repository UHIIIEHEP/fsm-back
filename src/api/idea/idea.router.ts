import express, { Request, Response } from 'express';
import { userInfoMiddleware } from './../../middleware/user_info.middleware';

import { serverResponse } from './../../helper';
import ideaController from './idea.controller';
import {
  joiSchemaCreate,
  joiSchemaDelete,
  joiSchemaList,
  joiSchemaStatusSet,
  joiSchemaUpdate,
} from './idea.schema';

const router = express.Router();

router.post('/create', userInfoMiddleware, async (req: Request, res: Response) => {
  try {
    await joiSchemaCreate.validateAsync(req.body)

    const data = await ideaController.create(req.body)

    serverResponse(req, res, data);
  } catch (err) {
    serverResponse(req, res, err, 'error');
  }
});

router.post('/delete', userInfoMiddleware, async (req: Request, res: Response) => {
  try {
    await joiSchemaDelete.validateAsync(req.body)

    const data = await ideaController.delete(req.body)

    serverResponse(req, res, data);
  } catch (err) {
    serverResponse(req, res, err, 'error');
  }
})

router.post('/list', userInfoMiddleware, async (req: Request, res: Response) => {
  try {
    await joiSchemaList.validateAsync(req.body)

    const data = await ideaController.list(req.body)

    serverResponse(req, res, data);
  } catch (err) {
    serverResponse(req, res, err, 'error');
  }
});

router.post('/status/set', userInfoMiddleware, async (req: Request, res: Response) => {
  try {
    await joiSchemaStatusSet.validateAsync(req.body)

    const data = await ideaController.statusSet(req.body)

    serverResponse(req, res, data);
  } catch (err) {
    serverResponse(req, res, err, 'error');
  }
});

router.post('/update', userInfoMiddleware, async (req: Request, res: Response) => {
  try {
    await joiSchemaUpdate.validateAsync(req.body)

    const data = await ideaController.update(req.body)

    serverResponse(req, res, data);
  } catch (err) {
    serverResponse(req, res, err, 'error');
  }
});

export default router;
