import ideaService from "./idea.service";


const ideaController = {
  create: async (payload: any) => {

    const idea = await ideaService.create(payload);

    return { idea };
  },

  delete: async (payload: any) => {

    const idea = await ideaService.delete(payload);

    return { idea };
  },

  list: async (payload: any) => {

    const idea = await ideaService.list(payload);

    return { idea };
  },

  statusSet: async (payload: any) => {

    const idea = await ideaService.statusSet(payload);

    return { idea };
  },

  update: async (payload: any) => {

    const idea = await ideaService.update(payload);

    return { idea };
  },
}

export default ideaController;