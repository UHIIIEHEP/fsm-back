import { IIdea, IStock } from "src/interface";

interface IFixture {
  stock: IStock,
  idea: IIdea,
}

const fixture: IFixture = {
  stock: {
    name: 'ПАО "ФСК ЕЭС"',
  },
  idea: {
    stock_id: 1,
    amount: 10,
    price_buy: 100,
    price_sale: 110,
    commission_percent: 0.03,
    userInfoBySession: {},
  },
}

export default fixture;
