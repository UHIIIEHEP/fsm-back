import { Request, Response } from 'express';
const jwt = require('jsonwebtoken');
require('dotenv').config();

import { databaseQuery, unAssocData } from './../helper';

const { Client } = require('pg')

const db = new Client({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
})
db.connect()

export const userInfoMiddleware = async (req: Request, res: Response, next: any) => {
  if (req.headers?.authorization) {
    const jwtToken = req.headers?.authorization?.split(' ');

    if (jwtToken[0].toLowerCase() !== 'bearer') {
      throw new Error('Authorization required (2)');
    }

    let decoded;

    try {
      decoded = jwt.verify(jwtToken[1], process.env.APP_USER_JWT_SIGNATURE);
    } catch (err) {
      throw new Error('Bad token');
    }

    const sessionId = decoded.session_id;

    const [userInfoBySession] = await databaseQuery(db, 'pkg_user', 'user_info_by_session', [
      sessionId
    ]);

    Object.assign(req.body, {
      userInfoBySession,
    });
  }

  next();
}
