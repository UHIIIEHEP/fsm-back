import { string } from "joi";

const commonResponse = {
  404: {
    description: 'Not found',
  }
}

export default commonResponse;
