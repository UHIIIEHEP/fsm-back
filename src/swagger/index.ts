import { swUserRoute } from '../api/user/swagger';
import { swIdeaRoute } from '../api/idea/swagger';
import { swStockRoute } from '../api/stock/swagger';

require('dotenv').config();

const port = Number(process.env.APP_PORT) || 4000;

const swagger = {
  openapi: '3.0.0',
  info: {
    title: 'Finance System Meter (FSM)',
    version: '1.0.0',
    description: 'The API for Finance System Meter (FSM)',
  },
  servers: [
    {
      url: `http://localhost:${port}`,
      description: 'Local server',
    }
  ],
  paths: {
    ...swIdeaRoute,
    ...swStockRoute,
    ...swUserRoute,
  },
}

export default swagger;