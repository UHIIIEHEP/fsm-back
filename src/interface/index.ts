interface IUserSelfInfo {
  user_id?: number,
  firstname?: string,
  lastname?: string,
  patronymic?: string,
  email?: string,
}

interface IUserAuth {
  email: string,
  password: string,
  address: string,
  client: string,
  salt?: string,
}

interface IStock {
  name: string,
}

interface IIdea {
  stock_id: number,
  amount: number,
  price_buy: number,
  price_sale: number,
  commission_percent: number,
  userInfoBySession: IUserSelfInfo,
};

interface IRolePermissionCheck {
  alias: string,
  relation: string,
  relation_id: number
  userInfoBySession: IUserSelfInfo,
}

export {
  IUserAuth,
  IUserSelfInfo,
  IStock,
  IIdea,
  IRolePermissionCheck,
}
