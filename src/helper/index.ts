import { Request, Response } from 'express';
const { Client } = require('pg')

const serverResponse = (req: Request, res: Response, data: any, status: string = 'ok') => {

  const message: {
    result?: object,
    error?: string,
    lvl: number,
    url?: string,
    body?: object,
    params?: object,
  } = {
    lvl: 30,
  };

  if (status === 'ok') {
    message.result = data;
  } else {
    message.error = data;
    message.lvl = 50;
    message.url = req.originalUrl;
    message.body = req.body;
    message.params = req.params;
  }

  console.log({ message })

  res.send(message);
}

const stripSlashes = (str: string) => {
  if (typeof str === 'string') {
    try {
      return str.replace(/\\(.)/g, '$1');
    } catch (err) {
      throw err;
    }
  }

  return str;
};


const databaseQuery = async (
  db: any,
  schema: string,
  func: string,
  params: any,
  namedObj = false,
): Promise<any> => {
  try {

    const procArguments: Array<any> = [];

    if (Array.isArray(params) && params.length > 0) {
      params.forEach((param) => {
        if (param !== null || typeof param !== 'undefined') {
          if (Array.isArray(param)) {
            if (param.length > 0) {
              procArguments.push(param);
            } else {
              procArguments.push(null);
            }
          } else {
            procArguments.push(stripSlashes(param));
          }
        } else {
          procArguments.push(null);
        }
      });
    }

    const args = procArguments.reduce((acc, item, index) => {
      acc.push(`$${index + 1}`);
      return acc;
    }, []);

    const query = `select * from ${schema}.${func}(${args.join(',')});`;

    const queryResult = await db.query(query, procArguments);

    if (!queryResult.rows.length) {
      return [];
    }

    // const result = namedObj ? queryResult : pgAssoc(queryResult);

    const result = queryResult.rows;

    return result;
  } catch (error) {
    console.log(error);
  }
};

const unAssocData = (data: Array<any>) => {
  const keys = Object.keys(data[0]);

  const body: Array<any> = data.map(item => {
    const arr: Array<any> = [];
    keys.forEach(key => {
      arr.push(item[key])
    })

    return arr;
  })

  return [keys, [...body]]
}


export {
  serverResponse,
  databaseQuery,
  unAssocData,
}
