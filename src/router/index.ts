
import userRouter from '../api/user/user.router';
import ideaRouter from '../api/idea/idea.router';
import stockRouter from '../api/stock/stock.router';
import roleRouter from '../api/role/role.router';

const router = (app: any) => {
  app.use('/idea', ideaRouter);
  app.use('/stock', stockRouter);
  app.use('/user', userRouter);
  app.use('/role', roleRouter);
}

export default router
