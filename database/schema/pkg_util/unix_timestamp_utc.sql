create or replace function "pkg_util"."unix_timestamp_utc"()
  returns integer as
$$
-- declare
--
--   v_stack               text;
--   v_fname               text;

begin

  return ceil(extract(epoch from now() at time zone 'utc'));

-- exception
--   when others then
--     if (not (sqlerrm ilike any (pkg_exception.message_list()))) then
--       get diagnostics v_stack = pg_context;
--       v_fname := substring(v_stack from 'function (.*?) line')::regprocedure::text;
--       execute pkg_log.exception_log_add(sqlstate, sqlerrm, current_query(), v_fname);
--       raise 'internal_error';
--     else
--       raise exception '%', sqlerrm;
--     end if;

end;

$$
  language plpgsql;