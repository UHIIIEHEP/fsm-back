create or replace function pkg_util.trigger_set_modified_utc()
  returns trigger as
$$
begin
    if new.modified is null then
        new.modified = pkg_util.timestamp_utc();
    end if;
    return new;
end;
$$
  language plpgsql volatile
  cost 100;
