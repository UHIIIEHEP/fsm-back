create or replace function pkg_util.timestamp_utc()
  returns "pg_catalog"."timestamp" as $$
  select now() at time zone 'utc';
$$
  language sql;

select pkg_util.timestamp_utc();
select pkg_util.unix_timestamp_utc();