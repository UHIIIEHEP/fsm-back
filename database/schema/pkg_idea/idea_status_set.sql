drop function if exists pkg_idea.idea_status_set (integer, integer, public.idea_status);

create or replace function pkg_idea.idea_status_set (
  p_user_id integer,
  p_idea_id  integer,
  p_status public.idea_status
)

returns integer
as $$

  begin

    -- check stock

    perform pkg_idea.idea_check(p_idea_id);

    -- set status

    update
      public.idea
    set
      status = p_status
    where
      idea_id = p_idea_id;

    return p_idea_id;

  end;
$$

language plpgsql;

-- select * from pkg_idea.idea_status_set (
--   1,
--   34,
--   'in_work'
-- )
