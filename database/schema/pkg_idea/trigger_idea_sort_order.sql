create or replace function pkg_idea.trigger_idea_sort_order()
  returns trigger as
$$
declare

  v_sort_order    integer;

begin

  if new.status != old.status then

    v_sort_order := (
      case
        when new.status = 'in_work' then 10
        when new.status = 'ready' then 20
        when new.status = 'release' then 30
        when new.status = 'archive' then 40

        else 10
      end
    );

    update
      public.idea
    set
      sort_order = coalesce(v_sort_order, sort_order)
    where
      idea_id = new.idea_id;

  end if;

  return new;

end;
$$
  language plpgsql;