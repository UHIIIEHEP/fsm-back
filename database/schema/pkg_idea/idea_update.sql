drop function if exists pkg_idea.idea_update (integer, integer, numeric, numeric, integer);

create or replace function pkg_idea.idea_update (
  p_user_id integer,
  p_idea_id  integer,
  p_price_sale numeric default null,
  p_price_buy numeric default null,
  p_amount integer default null
)

returns integer
as $$

  begin

    -- check stock

    perform pkg_idea.idea_check(p_idea_id);

    -- set status

    update
      public.idea
    set
      price_sale = coalesce(p_price_sale, price_sale),
      price_buy = coalesce(p_price_buy, price_buy),
      amount = coalesce(p_amount, amount)
    where
      idea_id = p_idea_id;

    return p_idea_id;

  end;
$$

language plpgsql;

-- select * from pkg_idea.idea_status_set (
--   1,
--   34,
--   'in_work'
-- )
