drop function if exists pkg_idea.idea_create (integer, integer, numeric, numeric, numeric);

create or replace function pkg_idea.idea_create (
  p_user_id integer,
  p_stock_id  integer,
  p_amount integer,
  p_price_buy numeric(17, 2),
  p_price_sale numeric(17, 2),
  p_commission_percent numeric(17, 2)
)

returns integer
as $$

  declare

    v_idea_id       integer;

  begin

    -- check stock

    perform pkg_stock.stock_check(p_stock_id);


    -- create

    insert into
      public.idea (
        user_id,
        stock_id,
        amount,
        price_buy,
        price_sale,
        commission_percent
      ) values (
        p_user_id,
        p_stock_id,
        p_amount,
        p_price_buy,
        p_price_sale,
        p_commission_percent
      ) returning idea_id into v_idea_id;


    return v_idea_id;

  end;
$$

language plpgsql;

-- select * from pkg_idea.idea_create (
--   1,
--   1,
--   10,
--   100,
--   110,
--   0.03
-- )