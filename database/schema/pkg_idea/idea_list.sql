drop function if exists pkg_idea.idea_list (integer, integer[], integer);

create or replace function pkg_idea.idea_list (
  p_user_id integer,
  p_idea_id integer[] default null,
  p_stock_id integer default null
)

returns table ( 
  row_number integer,
  idea_id integer,
  stock_id integer,
  stock_name character varying,
  amount integer,
  price_buy numeric,
  price_sale numeric,
  commission_percent numeric,
  commission_buy numeric,
  commission_sale numeric,
  status public.idea_status
)
as $$

  declare

  begin

    return query
      select
        (row_number() over())::integer as row_number,
        i.idea_id,
        i.stock_id,
        s.name,
        i.amount,
        i.price_buy,
        i.price_sale,
        i.commission_percent,
        (i.commission_percent * i.price_buy * i.amount / 100)::numeric(17, 2) as commission_buy,
        (i.commission_percent * i.price_sale * i.amount / 100)::numeric(17, 2) as commission_sale,
        i.status
      from
        public.idea i

        left join public.stock s using (stock_id)
      where
        i.deleted is null and
        i.user_id = p_user_id and

        case
          when cardinality(p_idea_id) > 0
            then i.idea_id = any(p_idea_id)
          else true
        end and

        case
          when p_stock_id is not null
            then i.stock_id = p_stock_id
          else true
        end

      order by
        i.sort_order;

  end;
$$

language plpgsql;
--
-- select * from pkg_idea.idea_list (
--   1,
--   '{}',
--   null
-- )