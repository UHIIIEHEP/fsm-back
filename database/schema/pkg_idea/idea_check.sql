-- drop function if exists pkg_idea.idea_check (integer, integer);
--
-- select * from pkg_idea.idea_check (2);

create or replace function pkg_idea.idea_check (
  p_idea_id  integer,
  p_allow_null boolean default false
)

returns integer
as $$

  declare

    v_idea_id      integer;

  begin

    if p_allow_null is true and p_idea_id is null then

      return null;

    end if;

    if p_allow_null is false and p_idea_id is null then

      raise 'idea_id_required';

    end if;

    -- check idea

    select
      s.idea_id
    into
      v_idea_id
    from
      public.idea s
    where
      s.idea_id = p_idea_id and
      s.deleted is null;

    if v_idea_id is null and p_allow_null = false then

      raise 'idea_id_not_found';

    end if;

    return v_idea_id;

  end;
$$

language plpgsql;
