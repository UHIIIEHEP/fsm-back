drop function if exists pkg_idea.idea_delete (integer, integer[]);

create or replace function pkg_idea.idea_delete (
  p_user_id integer,
  p_idea_id  integer[]
)

returns integer[]
as $$

  begin

    -- check stock

    perform pkg_idea.idea_check(unnest (p_idea_id));


    -- delete

    update
      public.idea
    set
      deleted = pkg_util.unix_timestamp_utc()
    where
      idea_id = any (p_idea_id);

    return p_idea_id;

  end;
$$

language plpgsql;

-- select * from pkg_idea.idea_delete (
--   1,
--   array[31, 33]
-- )