
create or replace function pkg_role.role_permission_check (
  p_user_id integer,
  p_relation public.relation,
  p_relation_id integer,
  p_alias character varying
)

returns boolean

as $$

  declare

    v_permission_id   integer;

  begin

--     perform pkg_user.user_check(p_user_id);

    select
      distinct (p.permission_id)
    into
      v_permission_id
    from
      public.user_relation_role_set urrs
      left join public.role_permission_set using (role_id)
      left join public.role_permission p using (permission_id)
    where
      urrs.user_id = p_user_id and
      urrs.relation = p_relation and
      urrs.relation_id = p_relation_id and
      p.alias = p_alias;

    return
      (case
        when v_permission_id is not null
          then true
        else false
      end);
  end;
$$

language plpgsql;
