-- select pkg_db_util.pg_constraint_exists('public', 'organisation', 'uk_organisation_name')::integer;

create or replace function pkg_db_util.pg_constraint_exists(
  p_schema character varying,
  p_table character varying,
  p_constraint character varying
)
returns boolean as
$$
declare

--   v_stack               text;
--   v_fname               text;

begin

    if not exists (
      select
        pgc.*
      from
        information_schema.table_constraints pgc
      where
        pgc.table_schema = p_schema and
        pgc.table_name = p_table and
        pgc.constraint_name = p_constraint
      )
    then

      return false;

    else

      return true;

    end if;

-- exception
--   when others then
--     if (not (sqlerrm ilike any (pkg_exception.message_list()))) then
--       get diagnostics v_stack = pg_context;
--       v_fname := substring(v_stack from 'function (.*?) line')::regprocedure::text;
--       execute pkg_log.exception_log_add(sqlstate, sqlerrm, current_query(), v_fname);
--       raise 'internal_error';
--     else
--       raise exception '%', sqlerrm;
--     end if;
end;
$$
  language plpgsql;
