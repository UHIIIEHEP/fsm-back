-- select * from pkg_db_util.list_all_table_trigger('public', 'schedule_fact_correction_data');

create or replace function pkg_db_util.list_all_table_trigger(
  p_schema character varying,
  p_table character varying
)
returns table (
  tg_name text,
  tg_schema text,
  tg_table text
)
as
$$

begin

  if p_schema is null or p_table is null then

    return;

  end if;

  return
    query
      select
        pgt.tgname::text,
        n.nspname::text,
        pgc.relname::text
      from
        pg_trigger pgt
      inner join
        pg_class pgc
          on pgc.oid = pgt.tgrelid
      inner join
        pg_namespace n
          on n.oid = pgc.relnamespace
      where
        pgt.tgisinternal is false and
        n.nspname = p_schema and
        pgc.relname = p_table;

exception
  when others then
    raise exception '%', sqlerrm;

end;
$$
  language plpgsql;
