create or replace function pkg_db_util.pg_type_exists(
  p_schema character varying,
  p_typename character varying
)
returns boolean as
$$
declare

--   v_stack               text;
--   v_fname               text;

begin

    if not exists (
      select
        t.*
      from
        pg_type t
      inner join pg_namespace ns
        on ns.oid = t.typnamespace
      where
        ns.nspname = p_schema and
        t.typname = p_typename
      )
    then

      return false;

    else

      return true;

    end if;

-- exception
--   when others then
--     if (not (sqlerrm ilike any (pkg_exception.message_list()))) then
--       get diagnostics v_stack = pg_context;
--       v_fname := substring(v_stack from 'function (.*?) line')::regprocedure::text;
--       execute pkg_log.exception_log_add(sqlstate, sqlerrm, current_query(), v_fname);
--       raise 'internal_error';
--     else
--       raise exception '%', sqlerrm;
--     end if;
end;
$$
  language plpgsql;
