-- select pkg_db_util.drop_all_table_trigger('public', 'schedule_fact_correction_data');

create or replace function pkg_db_util.drop_all_table_trigger(
  p_schema character varying,
  p_table character varying
)
returns void
as
$$
declare

  v_record record;

begin

  if p_schema is null or p_table is null then

    return;

  end if;

  for v_record in (
    select
      *
    from
      pkg_db_util.list_all_table_trigger(p_schema, p_table)
    )
  loop

    execute 'drop trigger if exists ' || quote_ident(v_record.tg_name) || ' on ' || quote_ident(p_schema) || '.' || quote_ident(p_table);

  end loop;

exception
  when others then
    raise exception '%', sqlerrm;

end;
$$
  language plpgsql;
