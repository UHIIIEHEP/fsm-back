create or replace function pkg_user.user_list (
  p_user_id integer[]
)
returns table (
  user_id integer,
  email character varying,
  firstname character varying,
  lastname character varying,
  patronymic character varying
)
as $$

begin

  return query
    select
      u.user_id,
      u.email,
      u.firstname,
      u.lastname,
      u.patronymic
    from
      public.user u
    where
      u.deleted is null and
      u.user_id = any(p_user_id);

end;
$$ language plpgsql;
