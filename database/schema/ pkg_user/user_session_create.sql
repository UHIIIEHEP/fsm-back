create or replace function pkg_user.user_session_create (
  p_user_id integer,
  p_address character varying,
  p_client character varying
)
returns integer as
$$
declare

  v_session_id	        integer;
--   v_stack               text;
--   v_fname               text;

begin

  -- check user

--   perform pkg_user.user_check(p_user_id);

  -- check session

  select
    s.session_id
  into
    v_session_id
  from
    public.session as s
  where
    s.address = p_address::inet and
    s.client = p_client and
    s.user_id = p_user_id and
    s.deleted is null;

  if v_session_id is not null then

  	return v_session_id;

  end if;

  -- insert

  insert into public.session (
    created_by,
    user_id,
    address,
    client
  )
  values (
    p_user_id,
    p_user_id,
    p_address::inet,
    p_client
   )

  returning session_id into v_session_id;

  return v_session_id;

-- exception
--   when others then
--     if (not (sqlerrm ilike any (pkg_exception.message_list()))) then
--       get diagnostics v_stack = pg_context;
--       v_fname := substring(v_stack from 'function (.*?) line')::regprocedure::text;
--       execute pkg_log.exception_log_add(sqlstate, sqlerrm, current_query(), v_fname);
--       raise 'internal_error';
--     else
--       raise exception '%', sqlerrm;
--     end if;
end;
$$
  language plpgsql;
