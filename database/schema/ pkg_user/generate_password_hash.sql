create or replace function pkg_user.generate_password_hash(
  p_password character varying,
  p_salt character varying
)
returns bytea
as
$$
declare

	v_hash       bytea;

--   v_stack      text;
--   v_fname      text;

begin

  if p_salt is null then

    raise 'password_check_error';

  end if;

  select hmac(hmac(p_password, p_salt, 'sha256')::character varying, p_salt, 'sha512') into v_hash;

	return v_hash;

-- exception
--   when others then
--     if (not (sqlerrm ilike any (pkg_exception.message_list()))) then
--       get diagnostics v_stack = pg_context;
--       v_fname := substring(v_stack from 'function (.*?) line')::regprocedure::text;
--       execute pkg_log.exception_log_add(sqlstate, sqlerrm, current_query(), v_fname);
--       raise 'internal_error';
--     else
--       raise exception '%', sqlerrm;
--     end if;
end;
$$
language plpgsql;
