create or replace function pkg_user.user_auth (
  p_email character varying,
  p_password character varying,
  p_address character varying,
  p_client character varying,
  p_salt character varying
)
returns integer as $$
declare

  v_salt                character varying;
  v_admin_id            integer;
  v_session_id          integer;
  v_admin_hash          bytea;

  v_user_id       integer;
  v_ps_hash       bytea;
  v_hash          bytea;

begin

  -- check user

  select
    u.user_id,
    u.ps_hash
  into
    v_user_id,
    v_ps_hash
  from
    public."user" u
  where
    u.deleted is null and
    u.email = p_email;

  if v_user_id is null then

    raise 'user_not_found';

  end if;

  -- check password

  v_hash := (select pkg_user.generate_password_hash(p_password, p_salt));

  if v_ps_hash != v_hash then

    raise 'password_mismatch';

  end if;

  select pkg_user.user_session_create(v_user_id, p_address, p_client) into v_session_id;

  return v_session_id;



--   -- check admin
--
--   select
--     a.admin_id,
--     a.ps_hash
--   into
--     v_admin_id,
--     v_admin_hash
--   from
--     pkg_admin.admin a
--   where
--     a.email = p_email and
--     a.deleted is null;
--
--   if v_admin_id is null then
--
--     raise 'admin_not_found';
--
--   end if;
--
--   -- get salt
--
--   if p_salt is null then
--
--     v_salt := (select current_setting('plpgsql.admin_password_salt', true));
--
--   else
--
--     v_salt := p_salt;
--
--   end if;
--
--   -- check password
--
--   select pkg_auth.generate_password_hash(p_password, v_salt) into v_hash;
--
--   if v_hash != v_admin_hash then
--
--     raise 'password_mismatch';
--
--   end if;
--
--   -- session
--
--   select pkg_admin.admin_session_create(v_admin_id, p_address, p_client) into v_session_id;
--
--   -- return result
--
--   return v_session_id;

--   return 2;

end;
$$ language plpgsql;
