create or replace function pkg_user.user_info_by_session(
  p_session_id integer
)
returns table (
  user_id integer,
  firstbame character varying,
  lastbame character varying,
  patronymic character varying,
  email character varying
)
as
$$
declare


--   v_stack      text;
--   v_fname      text;

begin

  return query
    select
      u.user_id,
      u.firstname,
      u.lastname,
      u.patronymic,
      u.email
    from
      public.session s

      left join public.user u using (user_id)
    where
      s.session_id = p_session_id;



-- exception
--   when others then
--     if (not (sqlerrm ilike any (pkg_exception.message_list()))) then
--       get diagnostics v_stack = pg_context;
--       v_fname := substring(v_stack from 'function (.*?) line')::regprocedure::text;
--       execute pkg_log.exception_log_add(sqlstate, sqlerrm, current_query(), v_fname);
--       raise 'internal_error';
--     else
--       raise exception '%', sqlerrm;
--     end if;
end;
$$
language plpgsql;
