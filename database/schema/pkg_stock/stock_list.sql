drop function if exists pkg_stock.stock_list (integer, integer[], integer);

create or replace function pkg_stock.stock_list (
  p_user_id integer,
  p_stock_id integer default null
)

returns table (
  stock_id integer,
  stock_name character varying
)
as $$

  declare

  begin

    return query
      select
        s.stock_id,
        s.name
      from
        public.stock s
      where
        s.deleted is null and

        case
          when p_stock_id is not null
            then s.stock_id = p_stock_id
          else true
        end;

  end;
$$

language plpgsql;

-- select * from pkg_stock.stock_list (
--   1,
--   null
-- )