-- drop function if exists pkg_stock.stock_check (integer, integer);
--
-- select * from pkg_stock.stock_check (2);

create or replace function pkg_stock.stock_check (
  p_stock_id  integer,
  p_allow_null boolean default false
)

returns integer
as $$

  declare

    v_stock_id      integer;

  begin

    if p_allow_null is true and p_stock_id is null then

      return null;

    end if;

    if p_allow_null is false and p_stock_id is null then

      raise 'stock_id_required';

    end if;

    -- check stock

    select
      s.stock_id
    into
      v_stock_id
    from
      public.stock s
    where
      s.stock_id = p_stock_id and
      s.deleted is null;

    if v_stock_id is null and p_allow_null = false then

      raise 'stock_id_not_found';

    end if;

    return v_stock_id;

  end;
$$

language plpgsql;
