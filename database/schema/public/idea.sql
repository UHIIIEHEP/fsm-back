do $$
  begin

    -- Sequence

    create sequence if not exists public.idea_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.idea
      (
        idea_id integer default nextval('public.idea_seq'::regclass) not null
      );

    alter sequence public.idea_seq owned by idea.idea_id;

    alter table public.idea add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.idea add column if not exists modified timestamp without time zone;
    alter table public.idea add column if not exists deleted integer;

    alter table public.idea add column if not exists user_id integer;
    alter table public.idea add column if not exists stock_id integer;
    alter table public.idea add column if not exists amount integer;
    alter table public.idea add column if not exists price_buy numeric;
    alter table public.idea add column if not exists price_sale numeric;
    alter table public.idea add column if not exists commission_percent numeric;
    alter table public.idea add column if not exists status public.idea_status default 'ready'::public.idea_status;
    alter table public.idea add column if not exists sort_order integer;
    alter table public.idea add column if not exists meta jsonb;

    -- Constraints

    alter table public.idea add
      constraint pk_idea_id primary key (idea_id);

    alter table public.idea add
      constraint fk_idea_stock foreign key (stock_id)
        references public.stock (stock_id) match simple
          on update cascade on delete cascade;

    -- Indexes

    create index if not exists idx_stock_id on public.idea (stock_id);

    -- Comments

    comment on sequence public.idea_seq
      is 'Idea id sequence';

    comment on table public.idea is 'Ideas';

    comment on column public.idea.idea_id
      is 'Idea id';

    comment on column public.idea.created
      is 'Time created UTC';

    comment on column public.idea.modified
      is 'Time modified UTC';

    comment on column public.idea.deleted
      is 'Time deleted UTC';

    comment on column public.idea.user_id
      is 'User id';

    comment on column public.idea.stock_id
      is 'Stock id';

    comment on column public.idea.amount
      is 'Amount';

    comment on column public.idea.price_buy
      is 'Price buy';

    comment on column public.idea.price_sale
      is 'Price sale';

    comment on column public.idea.commission_percent
      is 'Commission percent';

    comment on column public.idea.status
      is 'Idea status';

    comment on column public.idea.sort_order
      is 'Sort order';

    comment on column public.idea.meta
      is 'Meta';

    create trigger tr_set_modified
      before update
      on public.idea
      for each row
    execute procedure pkg_util.trigger_set_modified_utc();

    create trigger tr_idea_sort_order
      after insert or update
      on public.idea
      for each row
    execute procedure pkg_idea.trigger_idea_sort_order();

  end;
$$
