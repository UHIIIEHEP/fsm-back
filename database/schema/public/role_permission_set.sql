do $$
  begin

    -- Sequence

    create sequence if not exists public.role_permission_set_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.role_permission_set
    (
      role_permission_id integer default nextval('public.role_permission_set_seq'::regclass) not null
    );

    alter table public.role_permission_set add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.role_permission_set add column if not exists role_id integer;
    alter table public.role_permission_set alter column role_id set not null;
    alter table public.role_permission_set add column if not exists permission_id integer;
    alter table public.role_permission_set alter column permission_id set not null;

    alter sequence public.role_permission_set_seq owned by public.role_permission_set.role_permission_id;

    -- Constraints

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'role_permission_set', 'pk_role_permission_id')) = false) then

      alter table public.role_permission_set add
        constraint pk_role_permission_id primary key (role_permission_id);

    end if;

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'role_permission_set', 'uk_role_permission')) = false) then

      alter table public.role_permission_set add
        constraint uk_role_permission unique (role_id, permission_id);

    end if;

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'role_permission_set', 'fk_role_permission')) = false) then

      alter table public.role_permission_set add
        constraint fk_role_permission foreign key (permission_id)
          references public.role_permission (permission_id) match simple
          on update cascade on delete cascade;

    end if;

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'role_permission_set', 'fk_role')) = false) then

      alter table public.role_permission_set add
        constraint fk_role foreign key (role_id)
          references public.role (role_id) match simple
          on update cascade on delete cascade;

    end if;

    -- Indexes

    create index if not exists idx_role_permission_set_role_permission on public.role_permission_set (role_id, permission_id);

    -- Comments

    comment on sequence public.role_permission_set_seq
      is 'Role permission set id sequence';

    comment on table public.role_permission_set is 'Role permission set';

    comment on column public.role_permission_set.role_permission_id
      is 'Role permission set Id';

    comment on column public.role_permission_set.created
      is 'Time created UTC';

    comment on column public.role_permission_set.role_permission_id
      is 'Role permission set id';

    comment on column public.role_permission_set.role_id
      is 'Role id';

    comment on column public.role_permission_set.permission_id
      is 'Permission id';

  end
$$;
