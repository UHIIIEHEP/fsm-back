do $$
  begin

    -- Sequence

    create sequence if not exists public.user_relation_role_seq
    increment 1
    start 1
    minvalue 1
    no maxvalue;

    -- Structure

    create table if not exists public.user_relation_role_set
    (
      user_relation_role_id integer default nextval('public.user_relation_role_seq'::regclass) not null
    );

    alter sequence public.user_relation_role_seq owned by public.user_relation_role_set.user_relation_role_id;

    alter table public.user_relation_role_set add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.user_relation_role_set add column if not exists relation public.relation;
    alter table public.user_relation_role_set alter column relation set not null;
    alter table public.user_relation_role_set add column if not exists relation_id integer;
    alter table public.user_relation_role_set alter column relation_id set not null;
    alter table public.user_relation_role_set add column if not exists user_id integer;
    alter table public.user_relation_role_set alter column user_id set not null;
    alter table public.user_relation_role_set add column if not exists role_id integer;
    alter table public.user_relation_role_set alter column role_id set not null;

    -- Constraints

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'user_relation_role_set', 'pk_user_relation_role')) = false) then

      alter table public.user_relation_role_set
        add constraint pk_user_relation_role primary key (user_relation_role_id);

    end if;

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'user_relation_role_set', 'uk_user_relation_role_set')) = false) then

      alter table public.user_relation_role_set
        add constraint uk_user_relation_role_set unique (relation, relation_id, user_id, role_id);

    end if;

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'user_relation_role_set', 'fk_user_role_user')) = false) then

      alter table public.user_relation_role_set
        add constraint fk_user_role_user foreign key (user_id)
          references public.user (user_id) match simple
          on update cascade on delete cascade;

    end if;

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'user_relation_role_set', 'fk_user_role_role')) = false) then

      alter table public.user_relation_role_set
        add constraint fk_user_role_role foreign key (role_id)
          references public.role (role_id) match simple
          on update cascade on delete cascade;

    end if;

    -- Indexes

    create index if not exists idx_main on public.user_relation_role_set (relation, relation_id, role_id, user_id);
    create index if not exists idx_user on public.user_relation_role_set (user_id);
    create index if not exists idx_role on public.user_relation_role_set (role_id);

    -- Comments

    comment on sequence public.user_relation_role_seq
      is 'User relation role set id sequence';

    comment on table public.user_relation_role_set is 'User relation role set';

    comment on column public.user_relation_role_set.created
        is 'Time created UTC';

    comment on column public.user_relation_role_set.user_relation_role_id
        is 'User relation set id';

    comment on column public.user_relation_role_set.relation
        is 'Relation';

    comment on column public.user_relation_role_set.relation_id
        is 'Relation id';

    comment on column public.user_relation_role_set.user_id
        is 'User id';

    comment on column public.user_relation_role_set.role_id
        is 'Role id';

  end
$$;
