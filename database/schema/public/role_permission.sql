do $$
  begin

    -- Sequence

    create sequence if not exists public.role_permission_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.role_permission
    (
      permission_id integer default nextval('public.role_permission_seq'::regclass) not null
    );

    alter sequence public.role_permission_seq owned by public.role_permission.permission_id;

    alter table public.role_permission add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.role_permission add column if not exists modified timestamp without time zone;
    alter table public.role_permission add column if not exists created_by integer;
    alter table public.role_permission add column if not exists modified_by integer;
    alter table public.role_permission add column if not exists deleted integer;
    alter table public.role_permission add column if not exists alias character varying(50);
    alter table public.role_permission alter column alias set not null;
    alter table public.role_permission add column if not exists name character varying(255);
    alter table public.role_permission alter column name set not null;

    -- Constraints

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'role_permission', 'pk_permission')) = false) then

      alter table public.role_permission add
        constraint pk_permission primary key (permission_id);

    end if;

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'role_permission', 'uk_role_permission_alias')) = false) then

      alter table public.role_permission add
        constraint uk_role_permission_alias unique (alias);

    end if;

    -- Indexes

    create index if not exists idx_role_permission_alias on public.role_permission (alias);

    -- Comments

    comment on sequence public.role_permission_seq
      is 'Permission id sequence';

    comment on table public.role_permission is 'User permissions';

    comment on column public.role_permission.permission_id
      is 'Permission id';

    comment on column public.role_permission.created
      is 'Time created UTC';

    comment on column public.role_permission.created_by
      is 'Created by user id';

    comment on column public.role_permission.modified
      is 'Time modified UTC';

    comment on column public.role_permission.modified_by
      is 'Modified by user id';

    comment on column public.role_permission.deleted
      is 'Time deleted UTC';

    comment on column public.role_permission.alias
      is 'Permission alias';

    comment on column public.role_permission.name
      is 'Permission name';

    -- Triggers

    perform pkg_db_util.drop_all_table_trigger('public', 'role_permission');

    create trigger tr_set_modified
      before update
      on public.role_permission
      for each row
    execute procedure pkg_util.trigger_set_modified_utc();

  end
$$;
