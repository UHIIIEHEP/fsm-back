do $$
  begin

    -- Sequence

    create sequence if not exists public.role_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.role
    (
      role_id integer default nextval('public.role_seq'::regclass) not null
    );

    alter sequence public.role_seq owned by role.role_id;

    alter table public.role add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.role add column if not exists modified timestamp without time zone;
    alter table public.role add column if not exists created_by integer;
    alter table public.role add column if not exists modified_by integer;
    alter table public.role add column if not exists deleted integer;
    alter table public.role add column if not exists alias character varying(50); -- default pkg_util.generate_random_string(10);
    alter table public.role alter column alias set not null;
    alter table public.role add column if not exists name character varying(255);
    alter table public.role add column if not exists description character varying(255);

    -- Constraints

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'role', 'pk_role')) = false) then

      alter table public.role
        add constraint pk_role primary key (role_id);

    end if;

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'role', 'uk_role_alias')) = false) then

      alter table public.role add
        constraint uk_role_alias unique (alias);

    end if;

    -- Indexes

    create index if not exists idx_role_alias on public.role (alias);

    -- Comments

    comment on table public.role is 'User role';

    comment on column public.role.role_id
      is 'Role id';

    comment on column public.role.created
      is 'Time created UTC';

    comment on column public.role.created_by
      is 'Created by user id';

    comment on column public.role.modified
      is 'Time modified UTC';

    comment on column public.role.modified_by
      is 'Modified by user id';

    comment on column public.role.deleted
      is 'Time deleted UTC';

    comment on column public.role.alias
      is 'Role alias';

    comment on column public.role.name
      is 'Role name';

    comment on column public.role.description
      is 'Role description';

    -- Triggers

    perform pkg_db_util.drop_all_table_trigger('public', 'role');

    create trigger tr_set_modified
      before update
      on public.role
      for each row
    execute procedure pkg_util.trigger_set_modified_utc();

  end
$$;
