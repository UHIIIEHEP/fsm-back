do $$
  begin

    if ((select * from pkg_db_util.pg_type_exists('public', 'idea_status')) = false) then

      create type public.idea_status as enum();

    end if;

    alter type public.idea_status add value if not exists 'in_work';
    alter type public.idea_status add value if not exists 'ready';
    alter type public.idea_status add value if not exists 'release';
    alter type public.idea_status add value if not exists 'archive';

    comment on type public.idea_status
        is 'Idea status';

  end
$$;