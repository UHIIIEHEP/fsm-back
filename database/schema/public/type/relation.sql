do $$
  begin

    if ((select * from pkg_db_util.pg_type_exists('public', 'relation')) = false) then

      create type public.relation as enum();

    end if;

    alter type public.relation add value if not exists 'user';

    comment on type public.relation
        is 'Relation';

  end
$$;