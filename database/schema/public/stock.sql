do $$
  begin

    -- Sequence

    create sequence if not exists public.stock_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.stock
      (
        stock_id integer default nextval('public.stock_seq'::regclass) not null
      );

    alter sequence public.stock_seq owned by stock.stock_id;

    alter table public.stock add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.stock add column if not exists created_by integer;
    alter table public.stock add column if not exists modified timestamp without time zone;
    alter table public.stock add column if not exists modified_by integer;
    alter table public.stock add column if not exists deleted integer;

    alter table public.stock add column if not exists name character varying;
    alter table public.stock add column if not exists current_price numeric(17, 2);
    alter table public.stock add column if not exists meta jsonb;

    -- Constraints

    alter table public.stock add
      constraint pk_stock_id primary key (stock_id);

    -- Indexes


    -- Comments

    comment on sequence public.stock_seq
      is 'Stock id sequence';

    comment on table public.stock is 'stocks';

    comment on column public.stock.stock_id
      is 'Stock id';

    comment on column public.stock.created
      is 'Time created UTC';

    comment on column public.stock.created_by
      is 'Created by user id';

    comment on column public.stock.modified
      is 'Time modified UTC';

    comment on column public.stock.modified_by
      is 'Modified by user id';

    comment on column public.stock.deleted
      is 'Time deleted UTC';

    comment on column public.stock.name
      is 'Name';

    comment on column public.stock.current_price
      is 'Current_price';

    comment on column public.stock.meta
      is 'Meta';

    create trigger tr_set_modified
      before update
      on public.stock
      for each row
    execute procedure pkg_util.trigger_set_modified_utc();

  end;
$$
