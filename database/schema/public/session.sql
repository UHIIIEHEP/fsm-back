do $$
  begin

    -- Sequence

    create sequence if not exists public.session_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.session
    (
      session_id integer default nextval('public.session_seq'::regclass) not null
    );

    alter sequence public.session_seq owned by public.session.session_id;

    alter table public.session add column if not exists user_id integer;
    alter table public.session alter column user_id set not null;
    alter table public.session add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.session add column if not exists modified timestamp without time zone;
    alter table public.session add column if not exists created_by integer;
    alter table public.session add column if not exists modified_by integer;
    alter table public.session add column if not exists deleted integer;
    alter table public.session add column if not exists address inet;
    alter table public.session alter column address set not null;
    alter table public.session add column if not exists client character varying(255);

    -- Constraints

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'session', 'pk_session_id')) = false) then

      alter table public.session
        add constraint pk_session_id primary key (session_id);

    end if;

    if ((select * from pkg_db_util.pg_constraint_exists('public', 'session', 'fk_session_user')) = false) then

      alter table public.session
        add constraint fk_session_user foreign key (user_id)
          references public.user (user_id) match simple
            on update cascade on delete cascade;

    end if;

    -- Indexes

    create index if not exists idx_session_user on public.session (user_id);

    -- Comments

    comment on sequence public.session_seq
      is 'Session id sequence';

    comment on table public.session
      is 'User sessions';

    comment on column public.session.session_id
        is 'Session id';

    comment on column public.session.user_id
        is 'User id';

    comment on column public.session.created
      is 'Time created UTC';

    comment on column public.session.created_by
      is 'Created by user id';

    comment on column public.session.modified
      is 'Time modified UTC';

    comment on column public.session.modified_by
      is 'Modified by user id';

    comment on column public.session.deleted
      is 'Time deleted UTC';

    comment on column public.session.address
        is 'IP address which was used for session';

    comment on column public.session.client
        is 'Client, that was used for session';

    -- Triggers

    perform pkg_db_util.drop_all_table_trigger('public', 'session');

    create trigger tr_set_modified
      before update
      on public.session
      for each row
    execute procedure pkg_util.trigger_set_modified_utc();

  end
$$;
