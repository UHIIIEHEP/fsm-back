do $$
  begin

    insert into
      public.role_permission
        (created_by, alias, name)
      values


        (-1, 'user.idea.read', 'Пользователь. Идеи. Просмотр'),
        (-1, 'user.idea.write', 'Пользователь. Идеи. Создание/изменение')

    on conflict do nothing;

  end
$$;
