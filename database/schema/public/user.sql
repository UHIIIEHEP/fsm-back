do $$
  begin

    -- Sequence

    create sequence if not exists public.user_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public."user"
      (
        user_id integer default nextval('public.user_seq'::regclass) not null
      );

    alter sequence public.user_seq owned by "user".user_id;

    alter table public."user" add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public."user" add column if not exists modified timestamp without time zone;
    alter table public."user" add column if not exists deleted integer;

    alter table public."user" add column if not exists email character varying(150);
    alter table public."user" alter column email set not null;

    alter table public."user" add column if not exists ps_hash bytea;
    alter table public."user" alter column ps_hash set not null;


    alter table public."user" add column if not exists firstname character varying(50) default null;
    alter table public."user" add column if not exists lastname character varying(100) default null;
    alter table public."user" add column if not exists patronymic character varying(100) default null;

    alter table public."user" add column if not exists meta jsonb;


    -- Constraints

    alter table public."user" add
      constraint pk_user_id primary key (user_id);


    alter table public."user" add
      constraint uk_user_email unique (email);


    -- Indexes

    create index if not exists idx_user_email on public."user" (email);


    -- Comments

    comment on sequence public.user_seq
      is 'User id sequence';

    comment on table public."user" is 'Users';

    comment on column public."user".user_id
      is 'User id';

    comment on column public."user".created
      is 'Time created UTC';

    comment on column public."user".modified
      is 'Time modified UTC';

    comment on column public."user".deleted
      is 'Time deleted UTC';

    comment on column public."user".email
      is 'Email';

    comment on column public."user".firstname
      is 'Firstname';

    comment on column public."user".lastname
      is 'Lastname';

    comment on column public."user".patronymic
      is 'Patronymic';

    comment on column public."user".ps_hash
      is 'Hash';

    comment on column public."user".meta
      is 'Meta';


    -- trigger

    create trigger tr_set_modified
      before update
      on public."user"
      for each row
    execute procedure pkg_util.trigger_set_modified_utc();

  end;
$$
