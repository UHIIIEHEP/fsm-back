import express from 'express';
import swaggerUI from 'swagger-ui-express'
import swDocument from './src/swagger';
import bodyParser from 'body-parser';
import cors from 'cors';

import router from './src/router';

require('dotenv').config()

const port = Number(process.env.APP_PORT) || 4200;

const app = express();

app.use(cors())
app.use(bodyParser.json())

app.use('/docs', swaggerUI.serve, swaggerUI.setup(swDocument))

router(app);

app.listen(port, () => {
  console.log(`Server run. Port ${port}`);
})
